# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 13:15:59 2020

@author: jackb
"""

from PIL import Image, ImageDraw
import argparse


def main(args):
    # Begin by loading in the args
    im1 = Image.open(args.image_file_name1)
    im2 = Image.open(args.image_file_name2)
    mask = Image.new("L", im1.size, 0)

    draw = ImageDraw.Draw(mask)

    im = alternateRings(im1, im2, mask, draw)
    path = "images\ellipseConcentric.jpg"
    im.save(path)
    im.show()


def alternateRings(im1, im2, mask, draw):
    # center = (im2.size[0]//2, im2.size[1]//2)
    radius = 10
    xStart = radius
    xEnd = im2.size[0] - radius
    yStart = im2.size[1] - radius
    yEnd = radius

    while xStart < xEnd:
        # Using ellipse will generate distroted bars with heights proportional to distance from center
        draw.ellipse((xStart, yStart, xEnd, yEnd), fill=255)
        print("Draw")
        im = Image.composite(im2, im1, mask)
        print("composite")
        xStart += radius
        xEnd -= radius
        yStart -= radius
        yEnd += radius
    return im


if __name__ == "__main__":
    # Insert arg parser
    parser = argparse.ArgumentParser(description='Produce checkerboard.')
    parser.add_argument('image_file_name1', type=str, help='an image for the drawing the rings')
    parser.add_argument('image_file_name2', type=str, help='an image to be drawn onto')

    args = parser.parse_args()

    main(args)
